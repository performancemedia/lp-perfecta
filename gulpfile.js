var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var sourceMaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var htmlReplace = require('gulp-html-replace');
var htmlMin = require('gulp-htmlmin');
var imagemin = require('gulp-imagemin');
var changed = require('gulp-changed');
var replacePath = require('gulp-replace-path');
var concat = require('gulp-concat');
var merge = require('merge-stream');

gulp.task('reload', function(){
	browserSync.reload();
});

gulp.task('serve', ['html','sass','js'], function() {
	browserSync({
		server: 'dist'
	});
	gulp.watch('src/*.html', ['reload','html']);
	gulp.watch('src/sass/*.sass', ['sass']);
	gulp.watch('src/js/*.js', ['js']);
});

gulp.task('sass', function() {

  var csslearn = gulp.src('src/css/**/*')
    .pipe(concat('css-files.css'));

	var sasslearn = gulp.src('src/sass/**/*')
		.pipe(sourceMaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
	      browsers: ['last 3 versions']
	    }))
		.pipe(sourceMaps.write())
    .pipe(concat('sass-files.sass'));

  var mergedStream = merge(csslearn, sasslearn)
    .pipe(concat('style.min.css'))
    .pipe(cleanCSS())
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.stream());

  return mergedStream;
});

gulp.task('js', function() {
  return gulp.src('src/js/**/*.js')
  .pipe(uglify())
  .pipe(rename({
  	suffix: '.min'
  }))
  .pipe(gulp.dest('dist/js'))
})

gulp.task('html', function() {
  return gulp.src('src/*.html')
  .pipe(htmlReplace({
    'css':'css/style.min.css',
    'js':'js/script.min.js'
  }))
  .pipe(htmlMin({
    sortAttributes: true,
    sortClassName: true,
    // collapseWhitespace: true,
    removeComments: true
  }))
  .pipe(gulp.dest('dist'))
});

gulp.task('img', function() {
  return gulp.src('src/img/**/*.{jpg,jpeg,png,gif,mp4,svg,ico}')
  .pipe(changed('dist'))
  .pipe(imagemin())
  .pipe(gulp.dest('dist/img'))
});

gulp.task('doc', function() {
  return gulp.src('src/pdf/**/*.pdf')
  .pipe(gulp.dest('dist/pdf'))
});

gulp.task('video', function() {
  return gulp.src('src/video/**/*.mp4')
  .pipe(gulp.dest('dist/video'))
});

gulp.task('default', ['img','doc','video','serve']);