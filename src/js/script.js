$(document).ready(function(){
  var wow = new WOW({ mobile: false });
  wow.init();

  $('a.scroll[href^="#"]').bind('click.scroll', function(e) {
    var target;
    e.preventDefault();
    target = this.hash;
    return $('html, body').stop().animate({
      'scrollTop': ($(target).offset().top)
    }, 750, 'swing', function() {});
  });
});

window.addEventListener("load", function(){
  window.cookieconsent.initialise({
    "palette": {
      "popup": {
        "background": "#efefef",
        "text": "#404040"
      },
      "button": {
        "background": "#8ec760",
        "text": "#ffffff"
      }
    },
    "content": {
      "message": "Ta strona wykorzystuje pliki cookies w&nbsp;celach statystycznych i&nbsp;reklamowych oraz w&nbsp;celu dostosowania naszych serwisów do indywidualnych potrzeb klientów. Zmiany ustawień dotyczących plików cookie można dokonać w dowolnej chwili modyfikując ustawienia przeglądarki.",
      "dismiss": "Rozumiem",
      "link": "Dowiedz się więcej",
      "href": "ciasteczka.html"
    }
  })});